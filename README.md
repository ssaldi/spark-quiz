Spark Personality Test Exercise
===========================================================

This is an app created for spark interview coding round, uses Android Architecture Components with Dagger 2.


Introduction
-------------

### Functionality
The app is composed of 1 main screen and one Thank you screen.
#### QuestionFragment
Allows you to answer questions one by one that are fetch from network call.

Each question is saved in the database in `SparkQuizQuestions` table. Next time we get data only from DB. Moving forward we can use hashing to fetch differential data

Current Category is shown hi-lighted in Breadcrumb like view that uses Recyclerview

Current category question progress is shown using StoriesProgressView a library to show current progress in Instagram style ( I have used this instead of step progress Indicator to support rapid fire like questions in future)

On last Question Answered API call is made and response saved to Firebase database


#### Architecture


We use simple MVVM with LiveData here.

Fragment is only used for rendering and initilizing UI  based on state from ViewModel.

I have used ViewPager2 for rendering question (Although we could have achieved current functionality without using Viewpager2 but used for following reasons)

1) If we want to allow user to move to previous questions this will come in handy
2) This is new in andorid, I wanted to take this opportunity to explore Viewpager2

Have used Factory Design pattern for creating OptionsView in Adapter onBindViewHolder, again I did not use RecyclerView viewType for follwing reason:

There is two level complexity in creating optionView, Each question can have different type of View (like , single select, multi select range etc.) also number of options will be dynamic. So to simplify View creation I have used factory pattern



##### ViewModel

ViewModel uses Repository for its data requirements, I have used State pattern to manage current state of user in Quiz

ViewModel  is registered with QuizContext using listeners, Quiz states update QuizContext (which maintains current category, current questions like states) QuizContext updates ViewModel which invokes new state on View (QuestionFragment)

all business logic is also verified from repository as well


#### Supporting Multiple Screen sizes

No hardcoded sizes used all dimes referred from [intuit sdp] [intuit sdp]



#### Handling Orientation changes

1) Using View Models

2) Separate Layout according to needs of landscape and portrait modes



### Screenshots


![alt tag](https://bitbucket.org/ssaldi/spark-quiz/raw/a57a4785cb2573b384fd7872e329153e3f1470ee/screenshots/land.png)

![alt tag](https://bitbucket.org/ssaldi/spark-quiz/raw/a57a4785cb2573b384fd7872e329153e3f1470ee/screenshots/portrait.png)


### APK

* [Debug apk][debug-apk]



### Libraries
* [Android Support Library][support-lib]
* [Android Architecture Components][arch]
* [Android Data Binding][data-binding]
* [Dagger 2][dagger2] for dependency injection
* [Retrofit][retrofit] for REST api communication
* [espresso][espresso] for UI tests
* [mockito][mockito] for mocking in tests


[support-lib]: https://developer.android.com/topic/libraries/support-library/index.html
[arch]: https://developer.android.com/arch
[data-binding]: https://developer.android.com/topic/libraries/data-binding/index.html
[espresso]: https://google.github.io/android-testing-support-library/docs/espresso/
[dagger2]: https://google.github.io/dagger
[retrofit]: http://square.github.io/retrofit
[mockito]: http://site.mockito.org
[intuit sdp]: https://github.com/intuit/sdp
[debug-apk]: https://bitbucket.org/ssaldi/spark-quiz/src/master/app-debug.apk





