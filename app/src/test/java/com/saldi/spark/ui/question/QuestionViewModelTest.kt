package com.saldi.spark.ui.question

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.saldi.spark.repository.QuestionRepository
import com.saldi.spark.util.TestUtil
import com.saldi.spark.util.mock
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.*

@RunWith(JUnit4::class)

class QuestionViewModelTest {

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private val questionRepository = mock(QuestionRepository::class.java)
    private val questionViewModel = QuestionViewModel(questionRepository)




    @Test
    fun beginQuizTest() {
        questionViewModel.quizState.observeForever(mock())
        questionViewModel.beginQuiz(TestUtil.createDBQuestion())
        assert(questionViewModel.quizState?.value==QuizViewStates.INIT_QUIZ)
    }


    @Test
    fun onAnswerTest() {
        questionViewModel.quizState.observeForever(mock())
        questionViewModel.beginQuiz(TestUtil.createDBQuestion())
        questionViewModel.onAnswer("foo")
        assert(questionViewModel.quizState?.value==QuizViewStates.NEXT_CATEGORY)
    }

    @Test
    fun updateCategoryTest() {
        questionViewModel.quizState.observeForever(mock())
        questionViewModel.beginQuiz(TestUtil.createDBQuestion())
        questionViewModel.quizContext.currentQuestion = 3
        questionViewModel.onAnswer("foo")
        questionViewModel.onAnswer("foo")
        assert(questionViewModel.quizState?.value==QuizViewStates.NEXT_CATEGORY)
    }


    @Test
    fun lastinQuizTest() {
        questionViewModel.quizState.observeForever(mock())
        questionViewModel.beginQuiz(TestUtil.createDBQuestion())
        questionViewModel.quizContext.currentCategoryName = "passion"
        questionViewModel.quizContext.currentQuestion = 4
        questionViewModel.onAnswer("foo")
        questionViewModel.onAnswer("foo")
        assert(questionViewModel.quizState?.value==QuizViewStates.SUBMIT)
    }

}




