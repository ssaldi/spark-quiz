package com.saldi.spark.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer

import com.saldi.spark.api.SparkService
import com.saldi.spark.db.QuestionsDao
import com.saldi.spark.db.SparkQuizQuestions
import com.saldi.spark.util.ApiUtil
import com.saldi.spark.util.InstantAppExecutors
import com.saldi.spark.util.TestUtil
import com.saldi.spark.util.mock
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.Mockito.never
import org.mockito.Mockito.verify

@RunWith(JUnit4::class)
class QuestionsRepositoryTest {
    private val questionDao = mock(QuestionsDao::class.java)
    private val questionService = mock(SparkService::class.java)
    private val repo = QuestionRepository(InstantAppExecutors(), questionDao, questionService)

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Test
    fun loadQuestions() {
        repo.loadQuestions()
        verify(questionDao).getQuestions()
    }

    @Test
    fun goToNetwork() {
        val dbData = MutableLiveData<List<SparkQuizQuestions>>()
        `when`(questionDao!!.getQuestions()).thenReturn(dbData)
        val question = TestUtil.createQuestion()

        val call = ApiUtil.successCall(question)
        `when`(questionService!!.getQuestions()).thenReturn(call)

        val observer = mock<Observer<Resource<List<SparkQuizQuestions>>>>()

        repo.loadQuestions().observeForever(observer)
        verify(questionService, never()).getQuestions()
        val updatedDbData = MutableLiveData<List<SparkQuizQuestions>>()
        `when`(questionDao.getQuestions()).thenReturn(updatedDbData)
        dbData.value = null
        verify(questionService).getQuestions()
    }

    @Test
    fun dontGoToNetwork() {
        val dbData = MutableLiveData<List<SparkQuizQuestions>>()
        val question = TestUtil.createDBQuestion()
        dbData.value = question
        `when`(questionDao!!.getQuestions()).thenReturn(dbData)
        val observer = mock<Observer<Resource<List<SparkQuizQuestions>>>>()
        repo.loadQuestions().observeForever(observer)
        verify(questionService, never()).getQuestions()
        verify(observer).onChanged(Resource.success(question))
    }
}