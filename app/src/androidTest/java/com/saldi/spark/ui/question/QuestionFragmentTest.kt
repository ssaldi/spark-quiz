package com.saldi.spark.ui.question

import android.view.View
import android.view.ViewGroup
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.saldi.spark.MainActivity
import com.saldi.spark.R
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.not
import org.hamcrest.TypeSafeMatcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class QuestionFragmentTest {


    @Rule
    @JvmField
    var mActivityRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun submit_without_selection() {

       // onView(allOf(withId(com.saldi.spark.R.id.submitAnswer), isDisplayed())).perform(click())

        onView(withId(com.saldi.spark.R.id.submitAnswer)).perform(click())

        onView(withId(com.saldi.spark.R.id.question)).perform()

        onView(withId(com.google.android.material.R.id.snackbar_text)).check(matches(withText(com.saldi.spark.R.string.error_no_selection)))

    }



    @Test
    fun submit_with_selection() {
        onView(withIndex(firstChildOf(withId(R.id.options)), 0)).perform(click());

        onView(withId(com.google.android.material.R.id.snackbar_text)).check(matches(withText(com.saldi.spark.R.string.error_no_selection)))


        /* onView(allOf(withId(com.saldi.spark.R.id.question),
                 isDisplayed())).check(matches(not (withText("How important is the gender of your partner?"))))
 */    }


    @Test
    fun check_category_change() {
        onView(withIndex(firstChildOf(withId(R.id.options)), 0)).perform(click());

        onView(allOf(withId(com.saldi.spark.R.id.question),
                isDisplayed())).check(matches(not (withText("How important is the gender of your partner?"))))
    }


    fun clickOnViewChild(viewId: Int) = object : ViewAction {
        override fun getConstraints() = null

        override fun getDescription() = "Click on a child view with specified id."

        override fun perform(uiController: UiController, view: View) =
            click().perform(uiController, view.findViewById<View>(viewId))
    }


    fun firstChildOf(parentMatcher: Matcher<View>): Matcher<View> {
        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("with first child view of type parentMatcher")
            }

            public override fun matchesSafely(view: View): Boolean {

                if (view.parent !is ViewGroup) {
                    return parentMatcher.matches(view.parent)
                }
                val group = view.parent as ViewGroup
                return parentMatcher.matches(view.parent) && group.getChildAt(0) == view

            }
        }
    }


    fun withIndex(matcher: Matcher<View>, index: Int): Matcher<View> {
        return object : TypeSafeMatcher<View>() {
            var currentIndex = 0

            override fun describeTo(description: Description) {
                description.appendText("with index: ")
                description.appendValue(index)
                matcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                return matcher.matches(view) && currentIndex++ == index
            }
        }
    }
}