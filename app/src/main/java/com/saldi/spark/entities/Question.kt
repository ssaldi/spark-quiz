package com.saldi.spark.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Question {

    @SerializedName("question")
    @Expose
    var question: String? = null
    @SerializedName("category")
    @Expose
    var category: String? = null
    @SerializedName("question_type")
    @Expose
    var questionType: QuestionType? = null

}