package com.saldi.spark.entities

import com.google.gson.JsonObject
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class QuestionType {

    @SerializedName("type")
    @Expose
    var type: String? = null

    @SerializedName(value = "options")
    @Expose
    var options: List<String>? = null
    @SerializedName(value = "range")
    @Expose
    var range: JsonObject? = null
    @SerializedName("condition")
    @Expose
    var condition: JsonObject? = null
}
