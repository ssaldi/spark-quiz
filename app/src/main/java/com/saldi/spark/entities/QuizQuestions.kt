package com.saldi.spark.entities


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class QuizQuestions {
    @SerializedName("questions")
    @Expose
    private var questions: List<Question>? = null

    fun getQuestions(): List<Question>? {
        return questions
    }
}