package com.saldi.spark.db

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName


@Entity
data class SparkQuizQuestions(

    @field:SerializedName("category")
    val category: String,
    @field:SerializedName("type")
    val type: String,
    @field:SerializedName("question")
    val question: String
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}