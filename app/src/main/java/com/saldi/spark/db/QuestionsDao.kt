package com.saldi.spark.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

/**
 * Interface for database access for Question related operations.
 */
@Dao
interface QuestionsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(question: SparkQuizQuestions)

    @Query("SELECT * FROM  SparkQuizQuestions")
    fun getQuestions(): LiveData<List<SparkQuizQuestions>>

    @Query("SELECT * FROM  SparkQuizQuestions")
    fun getQuestionsCategoryGroup(): LiveData<List<SparkQuizQuestions>>
}


