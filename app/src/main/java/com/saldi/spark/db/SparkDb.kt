package com.saldi.spark.db


import androidx.room.Database
import androidx.room.RoomDatabase

/**
 * Main database description.
 */
@Database(
    entities = [
        SparkQuizQuestions::class],
    version = 1,
    exportSchema = false
)
abstract class SparkDb : RoomDatabase() {

    abstract fun questionsDao(): QuestionsDao

}
