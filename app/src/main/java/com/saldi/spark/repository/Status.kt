package com.saldi.spark.repository

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}