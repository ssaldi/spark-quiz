package com.saldi.spark.repository

import androidx.lifecycle.LiveData
import com.google.gson.Gson
import com.saldi.spark.AppExecutors
import com.saldi.spark.api.SparkService
import com.saldi.spark.db.QuestionsDao
import com.saldi.spark.db.SparkQuizQuestions
import com.saldi.spark.entities.Question
import com.saldi.spark.entities.QuizQuestions
import com.saldi.spark.testing.OpenForTesting
import org.json.JSONObject
import javax.inject.Inject
import javax.inject.Singleton


@OpenForTesting
@Singleton
class QuestionRepository @Inject constructor(private val appExecutors: AppExecutors,
                                             private val questionsDao: QuestionsDao,
                                             private val sparkService: SparkService) {
    fun loadQuestions(): LiveData<Resource<List<SparkQuizQuestions>>> {
        return object : NetworkBoundResource<List<SparkQuizQuestions>, QuizQuestions>(appExecutors) {
            override fun saveCallResult(item: QuizQuestions) {
                item.getQuestions()?.forEach {
                    it.category?.let { it1 ->
                        it.questionType?.type?.let { it2 ->
                            SparkQuizQuestions(it1, it2, Gson().toJson(it))
                        }
                    }?.let { it2 ->
                        questionsDao.insert(it2)
                    }
                }
            }

            override fun shouldFetch(data: List<SparkQuizQuestions>?) = data == null || data.isEmpty()

            override fun loadFromDb() = questionsDao.getQuestions()

            override fun createCall() = sparkService.getQuestions()
        }.asLiveData()
    }

    fun submitSurveyResponse(jsonObject: JSONObject): LiveData<Resource<JSONObject>> {

        return object : PostResource<JSONObject>(appExecutors) {
            override fun createCall() = sparkService.submitResponse(jsonObject)
        }.asLiveData()
    }

    fun isRefreshRequired(answer: String, question: String): Boolean {
        val currentQuestion = Gson().fromJson(question, Question::class.java)
        if (answer == currentQuestion.questionType?.condition?.getAsJsonObject("predicate")?.getAsJsonArray("exactEquals")?.get(
                    1)?.asString
        ) {
            return true
        }
        return false
    }
}
