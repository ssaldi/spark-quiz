package com.saldi.spark.repository

import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.saldi.spark.AppExecutors
import com.saldi.spark.api.ApiEmptyResponse
import com.saldi.spark.api.ApiErrorResponse
import com.saldi.spark.api.ApiResponse
import com.saldi.spark.api.ApiSuccessResponse

abstract class PostResource<ResultType>
@MainThread constructor(private val appExecutors: AppExecutors) {

    private val result = MediatorLiveData<Resource<ResultType>>()

    init {
        result.value = Resource.loading(null)
        fetchFromNetwork()
    }

    @MainThread
    private fun setValue(newValue: Resource<ResultType>) {
        if (result.value != newValue) {
            result.value = newValue
        }

    }

    private fun fetchFromNetwork() {
        val apiResponse = createCall()

        result.addSource(apiResponse) { response ->
            result.removeSource(apiResponse)
            when (response) {
                is ApiSuccessResponse -> {
                    setValue(Resource.success(response.body))
                }
                is ApiEmptyResponse -> {
                    appExecutors.mainThread().execute {}
                }
                is ApiErrorResponse -> {
                    onPostFailed()
                    setValue(Resource.error(response.errorMessage, null))
                }
            }
        }
    }

    protected open fun onPostFailed() {}

    fun asLiveData() = result as LiveData<Resource<ResultType>>


    @MainThread
    protected abstract fun createCall(): LiveData<ApiResponse<ResultType>>
}
