package com.saldi.spark.api


import androidx.lifecycle.LiveData
import com.saldi.spark.entities.QuizQuestions
import org.json.JSONObject
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

/**
 * REST API access points
 */
interface SparkService {
    @GET("/fetchQuestions")
    fun getQuestions(): LiveData<ApiResponse<QuizQuestions>>

    @POST("/submitAnswers")
    fun submitResponse(@Body jsonObject: JSONObject): LiveData<ApiResponse<JSONObject>>

}
