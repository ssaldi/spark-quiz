package com.saldi.spark.util

interface SparkConstants {
    interface QuestionEntity {
        companion object {
            val SINGLE_CHOICE_CONDITIONAL = "single_choice_conditional"
            val IF_POSITIVE = "if_positive"
            val QUESTION = "question"
        }
    }
}