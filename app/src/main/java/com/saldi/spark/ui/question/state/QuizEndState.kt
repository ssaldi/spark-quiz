package com.saldi.spark.ui.question.state

class QuizEndState : QuizState {
    override fun doAction(quizContext: QuizContext) {
        quizContext.submitResponse()
    }
}