package com.saldi.spark.ui.question.state

interface ViewStates {

    fun loadNextQuestion()

    fun loadNextCategory()

    fun updateQuestionSet()

    fun submitResponse()

    fun initQuiz()
}