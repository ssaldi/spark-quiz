package com.saldi.spark.ui.question.state

class QuizStartState : QuizState {
    override fun doAction(quizContext: QuizContext) {
        quizContext.initQuiz()
    }
}