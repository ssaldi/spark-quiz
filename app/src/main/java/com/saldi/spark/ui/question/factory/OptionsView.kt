package com.saldi.spark.ui.question.factory

import android.view.View

interface OptionsView {

    fun getOptionsView(): View

    fun getSelection(): String

}