package com.saldi.spark.ui.question.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.saldi.spark.R
import com.saldi.spark.databinding.ItemKrumbViewBinding
import com.saldi.spark.ui.common.BaseMultiItemAdapter
import com.saldi.spark.ui.common.BaseViewHolder


class KrumbViewHolder(val binding: ItemKrumbViewBinding) : BaseViewHolder(binding.root)

data class Krumb(var name: String, var isSelected: Boolean)

class KrumbsAdapter(itemsList: List<Krumb>) : BaseMultiItemAdapter<Krumb, BaseViewHolder>(itemsList) {

    override fun convert(helper: BaseViewHolder, item: Krumb) {
        (helper as KrumbViewHolder).apply {
            helper.binding.krumbText.text = item.name.replace("_", " ").toUpperCase()
            if (item.isSelected) {
                helper.binding.krumbText.setTextColor(ContextCompat.getColor( helper.binding.krumbText.context,R.color.spark_theme))
                helper.binding.krumbText.textSize = 14f
            } else {
                helper.binding.krumbText.setTextColor(ContextCompat.getColor(helper.binding.krumbText.context,R.color.spark_disabled_color))
                helper.binding.krumbText.textSize = 12f
            }
        }
    }


    override fun createHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return KrumbViewHolder(ItemKrumbViewBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }


}