package com.saldi.spark.ui.question.factory


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import com.jaygoo.widget.OnRangeChangedListener
import com.jaygoo.widget.RangeSeekBar
import com.saldi.spark.entities.QuestionType


class RangeOptionView(private var questionType: QuestionType, var context: Context) : OptionsView {
    var selectionText = ""
    override fun getSelection() = selectionText


    override fun getOptionsView(): View {

        val rangeView =
            (context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(com.saldi.spark.R.layout.range_option_view,
                    null)
        val rangeSeekBar = rangeView.findViewById<RangeSeekBar>(com.saldi.spark.R.id.rangeBar)

        questionType.range?.let { rangeSeekBar.setRange(it.get("from").asFloat, it.get("to").asFloat) }

        rangeSeekBar.setIndicatorTextDecimalFormat("0")

        rangeSeekBar.setOnRangeChangedListener(object : OnRangeChangedListener {
            override fun onRangeChanged(view: RangeSeekBar, leftValue: Float, rightValue: Float, isFromUser: Boolean) {
                selectionText = "$leftValue , $rightValue"
            }

            override fun onStartTrackingTouch(view: RangeSeekBar, isLeft: Boolean) {
            }

            override fun onStopTrackingTouch(view: RangeSeekBar, isLeft: Boolean) {
            }
        })
        val range = rangeSeekBar.maxProgress - rangeSeekBar.minProgress

        rangeSeekBar.setValue(rangeSeekBar.minProgress + range / 5, rangeSeekBar.maxProgress - range / 5)
        return rangeView
    }
}