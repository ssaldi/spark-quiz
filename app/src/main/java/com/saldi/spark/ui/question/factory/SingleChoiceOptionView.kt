package com.saldi.spark.ui.question.factory

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import com.saldi.spark.R
import com.saldi.spark.entities.QuestionType

class SingleChoiceOptionView(private var questionType: QuestionType, private var context: Context) : OptionsView {

    private var selectionText = ""

    override fun getSelection() = selectionText

    override fun getOptionsView(): View {
        val rgp = RadioGroup(context)
        questionType.options?.let {
            for (i in 0 until it.size) {
                val button = RadioButton(context)
                val buttonLayoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT)
                buttonLayoutParams.setMargins(0, 0, 0, 10)
                button.layoutParams = buttonLayoutParams
                button.id = i
                button.setTextColor(ContextCompat.getColor(button.context, R.color.spark_text_color))
                button.text
                button.text = it[i]
                button.setOnCheckedChangeListener { _, isChecked ->
                    if (isChecked) selectionText = it[i]
                }
                rgp.addView(button)
            }


        }
        return rgp
    }
}