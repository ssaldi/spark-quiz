package com.saldi.spark.ui.question.state

class NextState : QuizState {
    override fun doAction(quizContext: QuizContext) {
        if (quizContext.isLastInCategory) {
            // it is end of category. lets check if that ends quiz as well
            if (quizContext.isLastInQuiz) {
                // it is end of quiz lets set next category as QuizEndSatet
                quizContext.currentState = QuizEndState()
                quizContext.doAction()
            } else {
                // it is only end of category we still have  quiz lets load new category
                quizContext.currentState = CategoryEndState()
                quizContext.doAction()
            }
        } else {
            quizContext.loadNextQuestion()
        }

    }
}