package com.saldi.spark.ui.thank

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.saldi.spark.R
import kotlinx.android.synthetic.main.fragment_quiz.*

class ThankYouFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_quiz, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        progressActivity.showEmpty(ContextCompat.getDrawable(progressActivity.context, R.drawable.thank_you),
                resources.getString(R.string.thank_you),
                resources.getString(R.string.match))
    }


}