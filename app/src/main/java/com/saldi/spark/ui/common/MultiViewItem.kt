package com.saldi.spark.ui.common



abstract class BaseMultiItemAdapter<T : Any, K : BaseViewHolder>(val items: List<T>) :
    BaseRecyclerViewAdapter<T, K>(items.toMutableList()) {

    override fun getItemCount(): Int {
        return if (getEmptyViewCount() == 1) {
            1
        } else {
            mData.size
        }
    }

}