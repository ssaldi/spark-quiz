package com.saldi.spark.ui.question.state

interface QuizState {
    fun doAction(quizContext: QuizContext)
}