package com.saldi.spark.ui.common

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import com.saldi.spark.ui.common.BaseViewType.EMPTY_VIEW
import com.saldi.spark.ui.common.BaseViewType.LOADING_VIEW

object BaseViewType {
    const val LOADING_VIEW = 0x00000222
    const val EMPTY_VIEW = 0x00000555
}

interface BaseRVListener {
    var mItemClickListener: BaseRecyclerViewAdapter.OnItemClickListener?
    var mItemLongClickListener: BaseRecyclerViewAdapter.OnItemLongClickListener?
    var mItemChildClickListener: BaseRecyclerViewAdapter.OnItemChildClickListener?
}

interface RequestLoadMoreListener {
    fun onLoadMoreRequested()
}

/**
 * Ignoring warning un-used in this class as features like load ens etc may be required in future
 */
@Suppress("UNCHECKED_CAST")
abstract class BaseRecyclerViewAdapter<T, K : BaseViewHolder>(var mData: MutableList<T>) : RecyclerView.Adapter<K>(), BaseRVListener {

    interface OnItemClickListener {
        fun onItemClick(view: View, position: Int)
    }

    interface OnItemChildClickListener {
        fun onItemChildClick(view: View, position: Int)
    }

    interface OnItemLongClickListener {
        fun onItemLongClick(view: View, position: Int)
    }

    override var mItemClickListener: OnItemClickListener? = null

    override var mItemLongClickListener: OnItemLongClickListener? = null

    override var mItemChildClickListener: OnItemChildClickListener? = null

    var mRequestLoadMoreListener: RequestLoadMoreListener? = null

    private var mNextLoadEnable: Boolean = false
    private var mLoadMoreEnable: Boolean = false
    private var mLoading: Boolean = false

    var isShimmer: Boolean = false
    private var mObj: T? = null
    val EMPTY_SIZE = 10


    private var mRecyclerView: RecyclerView? = null

    private var mOpenAnimationEnable: Boolean = false


    private var mLastPosition: Int = -1



    protected abstract fun convert(helper: K, item: T)

    protected abstract fun createHolder(parent: ViewGroup, viewType: Int): BaseViewHolder

    fun getItem(position: Int): T {
        return mData[position]
    }

    override fun getItemCount(): Int {
        return if (getEmptyViewCount() == 1) {
            1
        } else {
            mData.size
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): K {
        var baseViewHolder: BaseViewHolder
        when (viewType) {
            EMPTY_VIEW -> {
                baseViewHolder = BaseViewHolder(mEmptyLayout as View)
            }
            else -> {
                baseViewHolder = createHolder(parent, viewType)
                bindViewClickListener(baseViewHolder)
            }
        }
        baseViewHolder.setAdapterListener(this)
        return baseViewHolder as K
    }

    fun setOnItemLongClickListener(listener: OnItemLongClickListener) {
        mItemLongClickListener = listener
    }

    fun setOnItemClickListener(listener: OnItemClickListener) {
        mItemClickListener = listener
    }

    private fun bindViewClickListener(baseViewHolder: BaseViewHolder?) {
        baseViewHolder?.let {
            baseViewHolder.itemView.setOnClickListener { v -> mItemClickListener?.onItemClick(v, baseViewHolder.layoutPosition) }
            baseViewHolder.itemView.setOnLongClickListener { v ->
                mItemLongClickListener?.onItemLongClick(v, baseViewHolder.layoutPosition)
                return@setOnLongClickListener true
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (getEmptyViewCount() == 1) {
            EMPTY_VIEW
        } else if (position < mData.size) {
            super.getItemViewType(position)
        } else {
            LOADING_VIEW
        }
    }

    private var mEmptyLayout: FrameLayout? = null

    fun setEmptyView(emptyView: View) {
        var insert = false
        if (mEmptyLayout == null) {
            mEmptyLayout = FrameLayout(emptyView.context)
            val layoutParams = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT)
            val lp = emptyView.layoutParams
            if (lp != null) {
                layoutParams.width = lp.width
                layoutParams.height = lp.height
            }
            mEmptyLayout?.layoutParams = layoutParams
            insert = true
        }
        mEmptyLayout?.removeAllViews()
        mEmptyLayout?.addView(emptyView)
        if (insert) {
            if (getEmptyViewCount() == 1) {
                notifyItemInserted(0)
            }
        }
    }

    fun getEmptyViewCount(): Int {
        if (mEmptyLayout == null || mEmptyLayout?.childCount == 0) {
            return 0
        }

        return if (mData.isNotEmpty()) {
            0
        } else 1
    }

    open fun getItemView(@LayoutRes layoutResId: Int, parent: ViewGroup): View {
        return LayoutInflater.from(parent.context).inflate(layoutResId, parent, false)
    }



    open fun createBaseViewHolder(parent: ViewGroup, layoutResId: Int): BaseViewHolder {
        return BaseViewHolder(getItemView(layoutResId, parent))
    }

    override fun onBindViewHolder(holder: K, position: Int) {
        var viewType = holder.itemViewType
        when (viewType) {
            0 -> convert(holder, getItem(position))
            EMPTY_VIEW -> {
            }
            else -> convert(holder, getItem(position))
        }
    }





    /**
     * @return Whether the Adapter is actively showing load
     * progress.
     */
    fun isLoading(): Boolean {
        return mLoading
    }



    fun addData(items: List<T>) {
        isShimmer = false
        mData.addAll(items)
        notifyDataSetChanged()
    }

    fun setData(items: List<T>) {
        isShimmer = false
        mData = items.toMutableList()
        notifyDataSetChanged()
    }
}