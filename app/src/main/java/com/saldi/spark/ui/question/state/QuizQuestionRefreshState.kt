package com.saldi.spark.ui.question.state

class QuizQuestionRefreshState : QuizState {
    override fun doAction(quizContext: QuizContext) {
        quizContext.updateQuestion()
    }
}