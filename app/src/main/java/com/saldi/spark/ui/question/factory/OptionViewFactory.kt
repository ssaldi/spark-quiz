package com.saldi.spark.ui.question.factory

import android.content.Context
import com.saldi.spark.entities.QuestionType


class OptionViewFactory {

    fun createOptionsView(questionType: QuestionType, context: Context): OptionsView {
        when (questionType.type) {
            "single_choice" -> return SingleChoiceOptionView(questionType, context)

            "single_choice_conditional" -> return SingleChoiceOptionView(questionType, context)

            "number_range" -> return RangeOptionView(questionType, context)


        }
        return SingleChoiceOptionView(questionType, context)
    }


}