package com.saldi.spark.ui.question

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.saldi.spark.db.SparkQuizQuestions
import com.saldi.spark.entities.Question
import com.saldi.spark.repository.QuestionRepository
import com.saldi.spark.repository.Resource
import com.saldi.spark.repository.Status
import com.saldi.spark.testing.OpenForTesting
import com.saldi.spark.ui.question.adapters.Krumb
import com.saldi.spark.ui.question.state.NextState
import com.saldi.spark.ui.question.state.QuizContext
import com.saldi.spark.ui.question.state.ViewStates
import com.saldi.spark.util.SparkConstants.QuestionEntity.Companion.IF_POSITIVE
import com.saldi.spark.util.SparkConstants.QuestionEntity.Companion.SINGLE_CHOICE_CONDITIONAL
import org.json.JSONObject
import javax.inject.Inject

enum class QuizViewStates {
    INIT_QUIZ, NEXT_QUESTION, NEXT_CATEGORY, REFRESH_QUESTIONS, SUBMIT, FINISH_QUIZ, FINISH_QUIZ_ERROR
}


@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
@OpenForTesting
class QuestionViewModel @Inject constructor(private var questionRepository: QuestionRepository) : ViewModel(),
    ViewStates {

    private lateinit var questionList: List<SparkQuizQuestions>

    private lateinit var quizCategoryMap: LinkedHashMap<String, List<SparkQuizQuestions>>

    var krumbsArray = ArrayList<Krumb>()

    lateinit var quizContext: QuizContext

    var questions: LiveData<Resource<List<SparkQuizQuestions>>> = MediatorLiveData<Resource<List<SparkQuizQuestions>>>()


    var quizState = MediatorLiveData<QuizViewStates>()

    private var answerMap = LinkedHashMap<String, String>()


    fun loadQuestions() {
        questions = questionRepository.loadQuestions()
    }

    fun getCurrentCategoryQuestions(): List<SparkQuizQuestions>? {
        return quizCategoryMap[krumbsArray[quizContext.currntCategory].name]

    }

    override fun initQuiz() {
        quizState.value = QuizViewStates.INIT_QUIZ
    }

    override fun loadNextQuestion() {
        quizState.value = QuizViewStates.NEXT_QUESTION
    }

    override fun loadNextCategory() {

        if (quizContext.currntCategory > 0) {
            var currentCategory = quizContext.currntCategory
            currentCategory--
            krumbsArray[currentCategory].isSelected = false
        }

        krumbsArray[quizContext.currntCategory].isSelected = true

        quizState.value = QuizViewStates.NEXT_CATEGORY
    }

    override fun updateQuestionSet() {
        val currentQuestion =
            Gson().fromJson(quizCategoryMap.get(quizContext.currentCategoryName)?.get(quizContext.currentQuestion)?.question,
                    Question::class.java)
        var currentIndex: Int = quizContext.currentQuestion
        val currentCategoryQuestions = ArrayList<SparkQuizQuestions>(getCurrentCategoryQuestions())

        currentIndex++
        currentCategoryQuestions.add(currentIndex,
                SparkQuizQuestions("",
                        currentQuestion.questionType?.type ?: "",
                        currentQuestion.questionType?.condition?.getAsJsonObject(IF_POSITIVE).toString()))
        quizCategoryMap[quizContext.currentCategoryName] = currentCategoryQuestions
        quizContext.currentQuestion++
        quizState.value = QuizViewStates.REFRESH_QUESTIONS
        quizContext.currentState = NextState()
        quizContext.isRefreshRequired = false
        //explicitly update question counter as we do not call next here

    }

    override fun submitResponse() {
        quizState.value = QuizViewStates.SUBMIT
    }


    fun beginQuiz(list: List<SparkQuizQuestions>) {
        this.questionList = list
        quizCategoryMap = questionList.groupBy { it.category } as LinkedHashMap<String, List<SparkQuizQuestions>>
        quizCategoryMap.keys.forEach { krumbsArray.add(Krumb(it, false)) }
        krumbsArray[0].isSelected = true
        quizContext = QuizContext(this, quizCategoryMap)
        quizContext.doAction()
    }


    fun onAnswer(answer: String) {
        answerMap[quizContext.currentCategoryName + quizContext.currentQuestion] = answer
        val sparkQuizQuestion = quizCategoryMap[quizContext.currentCategoryName]?.get(quizContext.currentQuestion)

        sparkQuizQuestion?.let {
            if (it.type == SINGLE_CHOICE_CONDITIONAL) quizContext.isRefreshRequired =
                questionRepository.isRefreshRequired(answer, it.question)
        }


        quizContext.doAction()
    }

    fun postResponse() {
        val jsonObject = JSONObject(Gson().toJson(answerMap))

        questionRepository.submitSurveyResponse(jsonObject).observeForever {
            when (it.status) {
                Status.SUCCESS -> {
                    quizState.value = QuizViewStates.FINISH_QUIZ
                }
                Status.ERROR -> {
                    quizState.value = QuizViewStates.FINISH_QUIZ_ERROR
                }
                else -> {
                    // no need to send loading as we already show loader
                }
            }
        }


    }

}
