package com.saldi.spark.ui.question.state

import com.saldi.spark.db.SparkQuizQuestions

class QuizContext(val viewStates: ViewStates, var quizCategoryMap: LinkedHashMap<String, List<SparkQuizQuestions>>) {


    var currentState: QuizState = QuizStartState()


    var currntCategory = 0

    var currentQuestion = 0

    var isRefreshRequired = false

    var currentCategoryName = ""

    var isLastInCategory = false

    var isLastInQuiz = false

    init {
        currentCategoryName = ArrayList(quizCategoryMap.keys)[currntCategory]
    }

    fun doAction() {
        if (currentState !is NextState) {
            currentState.doAction(this)
            currentState = NextState()
        } else {
            if (isRefreshRequired) currentState = QuizQuestionRefreshState()
            else currentState = NextState()
            currentState.doAction(this)
        }
    }


    fun loadNextQuestion() {
        currentQuestion++
        quizCategoryMap.get(currentCategoryName)?.size?.minus(1)
        if (currentQuestion == quizCategoryMap.get(currentCategoryName)?.size?.minus(1)) {

            isLastInCategory = true
            if (currentCategoryName == ArrayList(quizCategoryMap.keys).get(quizCategoryMap.keys.size - 1)) isLastInQuiz =
                true
        }
        viewStates.loadNextQuestion()
    }


    fun loadNextCategory() {
        currntCategory++
        currentCategoryName = ArrayList(quizCategoryMap.keys)[currntCategory]
        currentQuestion = 0
        isLastInCategory = false
        viewStates.loadNextCategory()
    }

    fun submitResponse() {
        viewStates.submitResponse()
    }

    fun initQuiz() {
        viewStates.initQuiz()
    }

    fun updateQuestion() {
        viewStates.updateQuestionSet()
    }


}