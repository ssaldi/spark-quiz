package com.saldi.spark.ui.question

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.saldi.spark.R
import com.saldi.spark.db.SparkQuizQuestions
import com.saldi.spark.di.Injectable
import com.saldi.spark.repository.Status
import com.saldi.spark.ui.question.adapters.KrumbsAdapter
import com.saldi.spark.ui.question.adapters.QuestionsAdapter
import com.saldi.spark.ui.question.adapters.SurveyListener
import kotlinx.android.synthetic.main.fragment_quiz.*
import javax.inject.Inject


enum class ViewErrors {
    ERROR_QUESTION, ERROR_ANSWER
}

@Suppress("WHEN_ENUM_CAN_BE_NULL_IN_JAVA")
class QuestionFragment : Fragment(), Injectable, SurveyListener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var questionViewModel: QuestionViewModel


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_quiz, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        progressActivity.showLoading()
        questionViewModel = ViewModelProviders.of(this, viewModelFactory).get(QuestionViewModel::class.java)

        questionViewModel.loadQuestions()

        questionViewModel.questions.observe(viewLifecycleOwner, Observer { result ->

            when (result.status) {
                Status.LOADING -> progressActivity.showLoading()
                Status.ERROR -> {
                    showError(ViewErrors.ERROR_QUESTION)
                }
                Status.SUCCESS -> result.data?.let {
                    if (questionViewModel.krumbsArray.isEmpty()) {
                        questionViewModel.beginQuiz(it)
                    }
                }
            }
        })


        questionViewModel.quizState.observe(viewLifecycleOwner, Observer { result ->
            if (viewPager.adapter == null) {
                progressActivity.showContent()
                krumbsView.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
                krumbsView.adapter = KrumbsAdapter(questionViewModel.krumbsArray)
                viewPager.adapter = questionViewModel.getCurrentCategoryQuestions()?.let { QuestionsAdapter(it, this) }
                questionViewModel.getCurrentCategoryQuestions()?.let { storiesProgressView.setStoriesCount(it.size) }

            } else {
                when (result) {
                    QuizViewStates.INIT_QUIZ -> initQuiz()
                    QuizViewStates.NEXT_QUESTION -> loadNextQuestion()
                    QuizViewStates.NEXT_CATEGORY -> loadNextCategory()
                    QuizViewStates.REFRESH_QUESTIONS -> refreshQuestions()
                    QuizViewStates.SUBMIT -> submitQuiz()
                    QuizViewStates.FINISH_QUIZ -> onQuizFinish()
                    QuizViewStates.FINISH_QUIZ_ERROR -> showError(ViewErrors.ERROR_ANSWER)
                }
            }
        })
        viewPager.isUserInputEnabled = false
    }

    private fun submitQuiz() {
        progressActivity.showLoading()
        questionViewModel.postResponse()
    }


    private fun refreshQuestions() {
        (viewPager.adapter as QuestionsAdapter).mData =
            questionViewModel.getCurrentCategoryQuestions() as MutableList<SparkQuizQuestions>
        viewPager.adapter?.notifyDataSetChanged()
        questionViewModel.getCurrentCategoryQuestions()?.let { storiesProgressView.setStoriesCount(it.size) }
        loadNextQuestion()
    }

    private fun loadNextCategory() {
        (viewPager.adapter as QuestionsAdapter).mData =
            questionViewModel.getCurrentCategoryQuestions() as MutableList<SparkQuizQuestions>
        viewPager.adapter?.notifyDataSetChanged()
        (krumbsView.adapter as KrumbsAdapter).mData = questionViewModel.krumbsArray
        krumbsView.adapter?.notifyDataSetChanged()
        loadFirstInCategory()
    }

    private fun loadNextQuestion() {
        val next = viewPager.currentItem + 1
        viewPager.currentItem = next
    }

    private fun loadFirstInCategory() {
        viewPager.setCurrentItem(0, false)
        questionViewModel.getCurrentCategoryQuestions()?.let { storiesProgressView.setStoriesCount(it.size) }

    }

    private fun initQuiz() {
        progressActivity.showContent()
        krumbsView.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        krumbsView.adapter = KrumbsAdapter(questionViewModel.krumbsArray)
        viewPager.adapter = questionViewModel.getCurrentCategoryQuestions()?.let { QuestionsAdapter(it, this) }
        questionViewModel.getCurrentCategoryQuestions()?.let { storiesProgressView.setStoriesCount(it.size) }

    }


    override fun onAnswerClicked(answer: String) {
        if (answer.isNotEmpty()) {
            questionViewModel.onAnswer(answer)
            storiesProgressView.updateStories()
        } else showSnackError(resources.getString(R.string.error_no_selection))
    }

    private fun showSnackError(message: String) {
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT).show()
    }

    private fun showError(error: ViewErrors) {
        progressActivity.showContent()
        var errorClickListener = View.OnClickListener {}
        when (error) {
            ViewErrors.ERROR_QUESTION -> {
                errorClickListener = View.OnClickListener {
                    //Here we can try again or anything we may want
                }
            }
            ViewErrors.ERROR_ANSWER -> {
                //Here we can try again or anything we may want
            }
        }

        progressActivity.showError(ContextCompat.getDrawable(progressActivity.context, R.drawable.icon_error),
                resources.getString(R.string.no_connection),
                resources.getString(R.string.error_detail),
                resources.getString(R.string.retry),
                errorClickListener)
    }

    private fun onQuizFinish() {
        findNavController().navigate(R.id.thankyouFragment)
    }

}

