package com.saldi.spark.ui.question.state

class SingleChoiceConditionalState : QuizState {
    override fun doAction(quizContext: QuizContext) {
        quizContext.loadNextQuestion()
    }
}