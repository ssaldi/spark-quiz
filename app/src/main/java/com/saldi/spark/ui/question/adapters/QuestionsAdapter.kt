package com.saldi.spark.ui.question.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import com.google.gson.Gson
import com.saldi.spark.databinding.ItemQuestionViewBinding
import com.saldi.spark.db.SparkQuizQuestions
import com.saldi.spark.ui.common.BaseMultiItemAdapter
import com.saldi.spark.ui.common.BaseViewHolder
import com.saldi.spark.ui.question.factory.OptionViewFactory


interface SurveyListener {
    fun onAnswerClicked(answer: String)
}


class OptionViewHolder(val binding: ItemQuestionViewBinding) : BaseViewHolder(binding.root)


class QuestionsAdapter(itemsList: List<SparkQuizQuestions>,private val surveyListener: SurveyListener) :
    BaseMultiItemAdapter<SparkQuizQuestions, BaseViewHolder>(itemsList) {

    override fun convert(helper: BaseViewHolder, item: SparkQuizQuestions) {

        (helper as OptionViewHolder).apply {
            val quizQuestion = Gson().fromJson(item.question, com.saldi.spark.entities.Question::class.java)
            val optionsView = quizQuestion.questionType?.let { OptionViewFactory().createOptionsView(it, binding.options.context) }
            binding.question.text = quizQuestion.question
            binding.options.removeAllViews()
            binding.options.addView(optionsView?.getOptionsView())

            binding.submitAnswer.setOnClickListener {
                optionsView?.getSelection()?.let { it1 -> surveyListener.onAnswerClicked(it1) }
            }
        }
    }


    override fun createHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return OptionViewHolder(ItemQuestionViewBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }


}