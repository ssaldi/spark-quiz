package com.saldi.spark.ui.question.state

class CategoryEndState : QuizState {
    override fun doAction(quizContext: QuizContext) {
        quizContext.loadNextCategory()
    }
}