package com.saldi.spark.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.saldi.spark.ui.question.QuestionViewModel
import com.saldi.spark.viewmodels.SparkViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(QuestionViewModel::class)
    abstract fun bindUserViewModel(questionViewModel: QuestionViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: SparkViewModelFactory): ViewModelProvider.Factory
}
