package com.saldi.spark.di

import com.saldi.spark.ui.question.QuestionFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class FragmentBuildersModule {
    @ContributesAndroidInjector
    abstract fun contributeQuestionFragment(): QuestionFragment

}
