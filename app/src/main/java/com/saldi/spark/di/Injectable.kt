package com.saldi.spark.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable
