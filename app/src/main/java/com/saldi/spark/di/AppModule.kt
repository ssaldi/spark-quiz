package com.saldi.spark.di

import android.app.Application
import androidx.room.Room
import com.saldi.spark.api.SparkService
import com.saldi.spark.db.QuestionsDao
import com.saldi.spark.db.SparkDb
import com.saldi.spark.util.LiveDataCallAdapterFactory
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor


@Module(includes = [ViewModelModule::class])
class AppModule {
    @Singleton
    @Provides
    fun provideSparkService(): SparkService {

        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BASIC
        val client = OkHttpClient.Builder().addInterceptor(logging).build()

        return Retrofit.Builder().baseUrl("https://us-central1-spark-networks-saldi.cloudfunctions.net/")
            .addConverterFactory(GsonConverterFactory.create()).client(client)
            .addCallAdapterFactory(LiveDataCallAdapterFactory()).build().create(SparkService::class.java)
    }

    @Singleton
    @Provides
    fun provideDb(app: Application): SparkDb {
        return Room.databaseBuilder(app, SparkDb::class.java, "spark.db").fallbackToDestructiveMigration().build()
    }

    @Singleton
    @Provides
    fun provideQuestionDao(db: SparkDb): QuestionsDao {
        return db.questionsDao()
    }
}
