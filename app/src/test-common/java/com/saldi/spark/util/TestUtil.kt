package com.saldi.spark.util

import com.google.gson.Gson
import com.saldi.spark.db.SparkQuizQuestions
import com.saldi.spark.entities.QuizQuestions
import okio.Okio

object TestUtil {
    fun createQuestion(): QuizQuestions {
        return QuizQuestions()
    }

    fun createDBQuestionOld(): List<SparkQuizQuestions>? {
        var sparkQuizQuestions = SparkQuizQuestions("", "", "")
        var sparkQuizArrayList = ArrayList<SparkQuizQuestions>()
        sparkQuizArrayList.add(sparkQuizQuestions)
        return sparkQuizArrayList
    }


    fun createDBQuestion(): List<SparkQuizQuestions> {
        var sparkQuestionList = ArrayList<SparkQuizQuestions>()
        val inputStream = javaClass.classLoader.getResourceAsStream("api-response/questions.json")
        val content = StringBuilder()
        val source = Okio.buffer(Okio.source(inputStream))
        var item = Gson().fromJson<QuizQuestions>(source.readString(Charsets.UTF_8), QuizQuestions::class.java)

        item.getQuestions()?.forEach {
            it.category?.let { it1 ->
                it.questionType?.type?.let { it2 ->
                    SparkQuizQuestions(it1, it2, Gson().toJson(it))
                }
            }?.let { it2 ->
                sparkQuestionList.add(it2)
            }
        }

        return sparkQuestionList
    }


}
